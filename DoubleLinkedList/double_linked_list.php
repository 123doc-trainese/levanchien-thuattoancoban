<?php

class ListNode
{
    public $val;
    public $pre;
    public $next;

    public function __construct(int $val = NULL)
    {
        $this->val = $val;
    }
}

class DoubleLinkedList
{
    private $head;
    private $tail;

    function addAtHead(int $val)
    {
        $newNode = new ListNode($val);
        if ($this->head === NULL) {
            $this->head = $newNode;
            $this->tail = $newNode;
        } else {
            $this->head->pre = $newNode;
            $newNode->next = $this->head;
            $this->head = $newNode;
        }
    }

    function addAtTail(int $val)
    {
        $newNode = new ListNode($val);
        if ($this->head === NULL) {
            $this->head = $newNode;
            $this->tail = $newNode;
        } else {
            $this->tail->next = $newNode;
            $newNode->pre = $this->tail;
            $this->tail = $this->tail->next;
        }
    }

    function addAtIndex(int $index, int $val)
    {
        $newNode = new ListNode($val);
        if ($this->head === NULL) {
            $this->head = $newNode;
            $this->tail = $newNode;
        } else {
            if ($index == 0) {
                $this->addAtHead($val);
                return;
            }
            $currentNode = $this->head;
            $index--;
            while ($index && $currentNode != NULL) {
                $currentNode = $currentNode->next;
                $index--;
            }
            if ($currentNode != NULL) {
                if ($currentNode === $this->tail) {
                    $this->addAtTail($val);
                    return;
                }
                $newNode->next = $currentNode->next;
                $currentNode->next->pre = $newNode;
                $currentNode->next = $newNode;
                $newNode->pre = $currentNode;
            } else {
                echo "Index is invalid";
            }
        }
    }

    function get(int $index)
    {
        $currentNode = $this->head;
        while ($index && $currentNode != NULL) {
            $currentNode = $currentNode->next;
            $index--;
        }
        if ($currentNode != NULL) {
            return $currentNode->val;
        } else {
            echo "Index is invalid";
        }
    }

    function deleteAtIndex(int $index)
    {
        if ($this->head == NULL) {
            echo "Linked List is empty";
            return;
        }
        if ($index == 0) {
            if ($this->head->next == NULL) {
                $this->head = NULL;
                $this->tail = NULL;
            } else {
                $this->head = $this->head->next;
                $this->head->pre = NULL;
            }
            return;
        }
        $currentNode = $this->head;
        $index--;
        while ($index && $currentNode->next != NULL) {
            $currentNode = $currentNode->next;
            $index--;
        }
        if ($currentNode->next != NULL) {
            if ($currentNode->next === $this->tail) {
                $currentNode->next = NULL;
                $this->tail = $currentNode;
                return;
            } else {
                // $tempNode = $currentNode->next;
                $currentNode->next = $currentNode->next->next;
                $currentNode->next->pre = $currentNode;
                // unset($tempNode);
            }
        } else {
            echo "Index is invalid" . "\n";
        }
    }

    function print()
    {
        $currentNode = $this->head;
        while ($currentNode !== NULL) {
            echo $currentNode->val . ' ';
            $currentNode = $currentNode->next;
        }
    }
    function reverse_print()
    {
        $currentNode = $this->tail;
        while ($currentNode !== NULL) {
            echo $currentNode->val . ' ';
            $currentNode = $currentNode->pre;
        }
    }
}
$linkedList = new DoubleLinkedList();
$linkedList->addAtHead(0);
$linkedList->addAtHead(-1);
$linkedList->addAtTail(1);
$linkedList->addAtIndex(3, 2); // 0 1 2 3
// $linkedList->addAtTail(3); // 0 1 2 3 
// $linkedList->addAtTail(4); // 0 1 2 3 4
// $linkedList->deleteAtIndex(0);
// $linkedList->deleteAtIndex(0);
// $linkedList->deleteAtIndex(1);
// $linkedList->deleteAtIndex(2);
$linkedList->print();
echo "\n";