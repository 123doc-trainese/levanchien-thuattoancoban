<?php
class ListNode
{
    public $val;
    public $next;

    public function __construct(int $val = NULL)
    {
        $this->val = $val;
    }
}

class MyLinkedList
{
    private $head;

    function addAtHead(int $val)
    {
        $newNode = new ListNode($val);
        if ($this->head === NULL) {
            $this->head = $newNode;
        } else {
            $newNode->next = $this->head;
            $this->head = $newNode;
        }
    }

    function addAtTail(int $val)
    {
        $newNode = new ListNode($val);
        if ($this->head === NULL) {
            $this->head = $newNode;
        } else {
            $currentNode = $this->head;
            while ($currentNode->next !== NULL) {
                $currentNode = $currentNode->next;
            }
            $currentNode->next = $newNode;
        }
    }

    function addAtIndex(int $index, int $val)
    {
        $newNode = new ListNode($val);
        if ($this->head === NULL) {
            $this->head = $newNode;
        } else {
            if ($index == 0) {
                $this->addAtHead($val);
                return;
            }
            $currentNode = $this->head;
            $index--;
            while ($index && $currentNode != NULL) {
                $currentNode = $currentNode->next;
                $index--;
            }
            if ($currentNode != NULL) {
                $newNode->next = $currentNode->next;
                $currentNode->next = $newNode;
            } else {
                echo "Index is invalid";
            }
        }
    }

    function get(int $index)
    {
        $currentNode = $this->head;
        while ($index && $currentNode != NULL) {
            $currentNode = $currentNode->next;
            $index--;
        }
        if ($currentNode != NULL) {
            return $currentNode->val;
        } else {
            echo "Index is invalid";
        }
    }

    function deleteAtIndex(int $index)
    {
        if ($this->head == NULL) {
            echo "Linked List is empty";
            return;
        }
        if ($index == 0) {
            if ($this->head->next == NULL) {
                $this->head = NULL;
            } else {
                $this->head = $this->head->next;
            }
            return;
        }
        $currentNode = $this->head;
        $index--;
        while ($index && $currentNode->next != NULL) {
            $currentNode = $currentNode->next;
            $index--;
        }
        if ($currentNode->next != NULL) {
            if ($currentNode->next->next == NULL) {
                $currentNode->next = NULL;
            } else {
                $currentNode->next = $currentNode->next->next;
            }
        } else {
            echo "Index is invalid" . "\n";
        }
    }
    function print()
    {

        $currentNode = $this->head;
        while ($currentNode !== NULL) {
            echo $currentNode->val . ' ';
            $currentNode = $currentNode->next;
        }
    }
}
$linkedList = new MyLinkedList();
// $linkedList->addAtHead(0); //0
// $linkedList->addAtTail(2); //0 2
// $linkedList->addAtIndex(1, 1); // 0 1 2
// $linkedList->addAtTail(3); // 0 1 2 3 
// $linkedList->addAtTail(4); // 0 1 2 3 4
// $linkedList->deleteAtIndex(0);
// $linkedList->deleteAtIndex(3);
// $linkedList->deleteAtIndex(1);
// $linkedList->deleteAtIndex(2);
$linkedList->print();
echo "\n";