# Bài 4: Định dạng văn bản

##Đề bài
Cho một mảng các từ `words` và chiều rộng `maxWidth`, hãy định dạng văn bản sao cho mỗi dòng có chính xác các `maxWidth` ký tự và được căn đều (trái và phải).

Bạn nên gói lời nói của mình theo cách tiếp cận tham lam; nghĩa là, đóng gói càng nhiều từ càng tốt trong mỗi dòng. Thêm khoảng trắng `' '` khi cần thiết để mỗi dòng có chính xác `maxWidth` ký tự.

Khoảng trống thừa giữa các từ nên được phân bố đồng đều nhất có thể. Nếu số lượng khoảng trắng trên một dòng không chia đều giữa các từ, các ô trống ở bên trái sẽ được gán nhiều khoảng trắng hơn các ô trống ở bên phải.

Đối với dòng cuối cùng của văn bản, nó phải được căn trái và không có khoảng trống thừa nào được chèn giữa các từ.

**Ghi chú:**

- Một từ được định nghĩa là một chuỗi ký tự chỉ bao gồm các ký tự không phải khoảng trắng.
- Độ dài của mỗi từ được đảm bảo lớn hơn `0` và không vượt quá `maxWidth`.
- Mảng đầu vào `words` chứa ít nhất một từ.

### Ví dụ 1

```
Input: words = ["This", "is", "an", "example", "of", "text", "justification."], maxWidth = 16
Output:
[
   "This    is    an",
   "example  of text",
   "justification.  "
]
```

### Ví dụ 2

```
Input: words = ["What","must","be","acknowledgment","shall","be"], maxWidth = 16
Output:
[
  "What   must   be",
  "acknowledgment  ",
  "shall be        "
]
Explanation: Note that the last line is "shall be    " instead of "shall     be", because the last line must be left-justified instead of fully-justified.
Note that the second line is also left-justified because it contains only one word.
```

### Ví dụ 3

```
Input: words = ["Science","is","what","we","understand","well","enough","to","explain","to","a","computer.","Art","is","everything","else","we","do"], maxWidth = 20
Output:
[
  "Science  is  what we",
  "understand      well",
  "enough to explain to",
  "a  computer.  Art is",
  "everything  else  we",
  "do                  "
]
```

### Điều kiện

- 1 <= `words.length` <= 300
- 1 <= `words[i].length` <= 20
- `words[i]` chỉ bao gồm các chữ cái và ký hiệu tiếng Anh.
- 1 <= `maxWidth` <= 100
- `words[i].length` <= `maxWidth`

# Hướng giải quyết:
- Duyệt qua các tử trong mảng `$words`, lần lượt nối các từ thành từng dòng sao cho độ dài mỗi dòng nhỏ hơn hoặc bằng `$maxWidth` và giữa các từ cách nhau 1 dấu cách. Ta thu được mảng `$lines` chứa các dòng.
- Duyệt qua các dòng trong mảng `$lines`, với mỗi dòng ta lần lượt thêm khoảng cách vào giữa các từ sao cho thỏa mãn yêu cầu đề bài. Đối với dòng cuối thì sẽ thêm khoảng cách vào sau từ cuỗi cùng sao cho đủ độ dài `$maxWidth`.
