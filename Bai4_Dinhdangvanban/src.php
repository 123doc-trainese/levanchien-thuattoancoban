<?php
$words = ["This", "is", "an", "example", "of", "text", "justification."];
$words = ["Science", "is", "what", "we", "understand", "well", "enough", "to", "explain", "to", "a", "computer.", "Art", "is", "everything", "else", "we", "do"];

$words = ["What", "must", "be", "acknowledgment", "shall", "be"];
if ($words == null) {
    echo [];
    return;
}
$lines = [];
$s = "";
$maxWidth = 16;
for ($i = 0; $i < count($words); $i++) {
    if (strlen($s) + strlen($words[$i]) <= $maxWidth) {
        $s .= $words[$i];
        if (strlen($s) < $maxWidth) {
            $s .= ' ';
        }
    } else {
        $s = trim($s);
        array_push($lines, $s);
        $s = $words[$i];
        if (strlen($s) < $maxWidth) {
            $s .= ' ';
        }
    }
    if ($i == count($words) - 1) {
        array_push($lines, $s);
    }
}

for ($i = 0; $i < count($lines) - 1; $i++) {
    $str = $lines[$i];
    $cnt = $maxWidth - strlen($str);
    $strArr = explode(' ', $str);
    $cnt += count($strArr) - 1;
    $j = 0;
    while ($cnt !== 0) {
        if ($j == count($strArr) - 1) {
            $j = 0;
        }
        $strArr[$j] .= ' ';
        $j++;
        $cnt--;
    }
    $str = implode('', $strArr);
    echo $str . "\n";
    $lines[$i] = $str;
}

while (strlen($lines[count($lines) - 1]) < $maxWidth) {
    $lines[count($lines) - 1] .= " ";
}
print_r($lines);
echo strlen($lines[0]) . "\n";
echo strlen($lines[1]) . "\n";
echo strlen($lines[2]) . "\n";