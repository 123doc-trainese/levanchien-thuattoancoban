# Bài 1: Tìm kiếm nhị phân

## Để bài

Cho một mảng các số nguyên `nums` được sắp xếp theo thứ tự không giảm, hãy tìm vị trí bắt đầu và kết thúc của một giá trị` target` nhất định.
Nếu không tìm thấy `target` trong mảng, hãy trả về`[-1, -1]`.

Bạn phải viết một thuật toán với độ phức tạp thời gian chạy `O (log n)`.

### Ví dụ 1:

```
Đầu vào: nums = [5,7,7,8,8,10], target = 8
Đầu ra: [3,4]
```

### Ví dụ 2:

```
Đầu vào: nums = [5,7,7,8,8,10], target = 6
Đầu ra: [-1, -1]
```

### Ví dụ 3:

```
Đầu vào: nums = [], target = 0
Đầu ra: [-1, -1]
```

### Điều kiện

- 0 <= `nums.length` <= 105
- -109 <= `nums [i]` <= 109
- `nums` là một mảng không giảm.
- -109 <= `target` <= 109

# Hướng giải quyết:
Với yêu cầu thuật toán với độ phức tạp trong thời gian chạy `O(log n)` nên ta sẽ dùng thuật toán tìm kiếm nhị phân. 
- Nếu không tìm thấy `target` trong mảng => `[-1,-1]`
- Nếu tìm thấy `target` trong mảng:
    - Tiếp tục tìm kiếm nhị phân để tìm chỉ số phần tử có giá trị lớn nhất nhỏ hơn `target` => Vị trí bắt đầu của `$target` là `$start = $index + 1`
    - Tìm tiếp chỉ số phần tử có giá trị nhỏ nhất lớn hơn `target` => Vị trí kết thúc của `$target` là `$end = $index - 1` => `[$start,$end]`
