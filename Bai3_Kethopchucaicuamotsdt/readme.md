# Bài 3: Kết hợp chữ cái của một số điện thoại

## Đề bài

Cho một chuỗi chứa các chữ số từ `2-9`, trả về tất cả các kết hợp chữ cái có thể có mà số đó có thể đại diện. Trả lại câu trả lời **theo bất kỳ thứ tự nào**.

Dưới đây là một ánh xạ các chữ số thành các chữ cái (giống như trên các nút điện thoại). Lưu ý rằng 1 không ánh xạ đến bất kỳ chữ cái nào.

![image.png](https://trello.com/1/cards/63058d82b2f99281539735ce/attachments/6309aaa8d2f4130157a8f196/download/image.png)

### Ví dụ 1:

```
Đầu vào: chữ số = "23"
Đầu ra: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"]
```

### Ví dụ 2:

```
Đầu vào: chữ số = ""
Đầu ra: []
```

### Ví dụ 3:

```
Đầu vào: chữ số = "2"
Đầu ra: ["a", "b", "c"]
```

### Điều kiện:

- 0 <= `digits.lenght` <= 4

- `digits[i]` là một chữ số trong phạm vi `['2', '9']`.

# Hướng giải quyết:
- Sử dụng thuật toán duyệt toàn bộ, lần lượt duyệt qua các chữ số của chuỗi, sau đó lần lượt ghép các chữ cái mà số đó đại diện với các chữ cái của số tiếp theo, mỗi khi thu được 1 kết quả phù hợp thì thêm vào mảng kết quả `$result` theo yêu cầu đề bài.
