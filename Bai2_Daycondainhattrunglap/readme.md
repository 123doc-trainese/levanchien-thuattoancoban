# Bài 2: Chuỗi con dài nhất không có ký tự lặp lại

## Đề bài

Cho một chuỗi `s`, tìm các _chuỗi con dài nhất_ có ký tự lặp lại.
### Ví dụ 1:

```
Đầu vào: s = "abcabcbb"
Đầu ra: [6,7]
Giải thích: Câu trả lời là "bb", với độ dài là 2.
```

### Ví dụ 2:

```
Đầu vào: s = "bbbbb"
Đầu ra: [0,4]
Giải thích: Câu trả lời là "b", với độ dài là 5.
```

### Ví dụ 3:

```
Đầu vào: s = "pwwkew"
Đầu ra: [1,2]
Giải thích: Câu trả lời là "ww", với độ dài là 2.
```

### Điều kiện:

- 0 <=` s.length` <= 5 \* 104
- `s` bao gồm các chữ cái tiếng Anh, chữ số, ký hiệu và dấu cách.

# Hướng giải quyết:
- Duyệt qua các phần tử của chuỗi, thêm lần lượt chỉ số bắt đầu và kết thúc `[$start, $end]` của các chuỗi con không trùng lặp vào mảng kết quả `$result`, nếu chuỗi con trùng lặp hiện tại có độ dài lớn hơn các chuỗi con trùng lặp trước đó thì sẽ làm rỗng mảng sau đó mới thêm chỉ số của chuỗi con trùng lặp hiện tại vào. Kết quả ta thu được mảng các chỉ số bắt đầu và kết thúc chuỗi con trùng lặp có độ dài lớn nhất `$result`. 